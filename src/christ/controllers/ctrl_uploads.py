from aiohttp import web

from christ.config import CONFIG


async def view_upload(request: web.Request) -> web.FileResponse:
    return web.FileResponse(CONFIG.paths.data / 'uploads' / request.match_info['filepath'])
