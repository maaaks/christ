from aiohttp import web
from orm.expressions.basic import Param
from orm.expressions.fn import fn
from orm.expressions.misc import Desc
from orm.queries.select import JoinType

from christ.models import Entry, Upload
from christ.models.blog import Blog
from christ.render import render


async def view_blog(request: web.Request) -> web.Response:
    username = request.match_info['username']
    page = int(request.query.get('page', '1'))
    page_size = 10

    blog = await (
        Blog
        .select(*Blog, *Blog.userpic)
        .join(Upload, Blog.userpic_id == Upload.id, JoinType.Left)
        .where(Blog.name == Param(username))
        .one())

    entries_count = (await (
        Entry
        .select(fn.count(Entry.id).as_('c'))
        .where(Entry.blog_id == Param(blog.id))
        .where(Entry.parent_id == None)
        .one()
    )).__dict__['c']

    _entries = (
        Entry
        .select(*Entry, fn.count_comments(Entry.id).as_('count_comments'))
        .where(Entry.blog == Param(blog.id))
        .where(Entry.parent_id == None)
        .order_by(Desc(Entry.created))
        .page(page, page_size))
    entries = []
    async for entry in _entries:
        entry.blog = blog
        entries.append(entry)

    return await render('page-blog.jinja2',
                        blog=blog, entries=entries, entries_count=entries_count,
                        page=page, page_size=page_size)


async def view_entry(request: web.Request) -> web.Response:
    username = request.match_info['username']
    local_id = int(request.match_info['local_id'])

    entry = await (
        Entry
        .select(*Entry, *Entry.blog, *Entry.blog.userpic,
                fn.count_comments(Entry.id).as_('count_comments'))
        .join(Blog, Entry.blog_id == Blog.id)
        .join(Upload, Entry.blog.userpic_id == Upload.id, JoinType.Left)
        .where(Blog.name == Param(username))
        .where(Entry.local_id == Param(local_id))
        .limit(1)
        .one())
    return await render('page-entry.jinja2', blog=entry.blog, entry=entry)
