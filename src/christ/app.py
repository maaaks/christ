#!/usr/bin/env python

from asyncio import get_event_loop
from dataclasses import asdict

from aiohttp import web
from orm.database import DbParams
from orm.engines.asyncpg import AsyncpgDatabase
from orm.model import ModelMetaClass

from christ.config import CONFIG
from christ.controllers import view_blog, view_entry, view_upload


app = web.Application()
app.add_routes([
    web.get('/favicon.ico', lambda _: web.FileResponse(CONFIG.paths.img / 'favicon.ico')),
    web.get('/style.css', lambda _: web.FileResponse(CONFIG.paths.css)),
    web.get('/uploads/{filepath:[^?#]+}', view_upload),
    web.static('/img', CONFIG.paths.img),

    web.get(r'/{username:[^/]+}', view_blog, name='blog'),
    web.get(r'/{username:[^/]+}/{local_id:\d+}', view_entry, name='entry'),
])


def main():
    ModelMetaClass.default_db = AsyncpgDatabase()
    with open(CONFIG.paths.sql / 'functions.sql') as functions_file:
        ModelMetaClass.default_db.execute(functions_file.read())

    get_event_loop().run_until_complete(
        ModelMetaClass.default_db.connect(DbParams(**asdict(CONFIG.database))))
    web.run_app(app)


if __name__ == '__main__':
    main()
