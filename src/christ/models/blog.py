from orm.fields.datetimefield import DateTimeField
from orm.fields.foreignkeyfield import ForeignKeyField
from orm.fields.integerfield import IntegerField
from orm.fields.stringfield import StringField
from orm.model import Model

from .upload import Upload


class Blog(Model, table='blogs'):
    id = IntegerField(primary_key=True)
    name = StringField(10)
    title = StringField(10)
    created = DateTimeField()
    description = StringField(10)

    userpic_id = IntegerField()
    userpic = ForeignKeyField(Upload, userpic_id)

    def url(self) -> str:
        from christ.app import app
        return app.router['blog'].url_for(username=self.name)
