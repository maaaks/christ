from orm.fields.integerfield import IntegerField
from orm.fields.stringfield import StringField
from orm.model import Model


class Upload(Model, table='uploads'):
    id = IntegerField(primary_key=True)
    directory = StringField(10)
    filename = StringField(300)

    def url(self) -> str:
        return f'/uploads/{self.directory}/{self.filename}'
