from __future__ import annotations

from datetime import datetime
from functools import cached_property
from subprocess import PIPE, Popen
from typing import List

from orm.fields.datetimefield import DateTimeField
from orm.fields.foreignkeyfield import ForeignKeyField
from orm.fields.integerfield import IntegerField
from orm.fields.stringfield import StringField
from orm.model import Model

from christ.models import Blog


class Entry(Model, table='entries'):
    id = IntegerField(primary_key=True)
    parent_id = IntegerField()
    created = DateTimeField()
    title = StringField(10)
    author_id = IntegerField()
    content = StringField(10)
    local_id = IntegerField()
    blog_id = IntegerField()
    blog = ForeignKeyField(Blog, blog_id)

    def __post_init__(self):
        if isinstance(self.date, str):
            self.date = datetime.strptime(self.date, '%Y-%m-%d %H:%M')

    def url(self) -> str:
        from christ.app import app
        return app.router['entry'].url_for(username=self.blog.name, local_id=str(self.local_id))

    @cached_property
    def body_html(self) -> str:
        process = Popen(['cmark-gfm', '-e', 'strikethrough', '-e', 'autolink', '-e', 'tagfilter'], stdin=PIPE, stdout=PIPE, text=True)
        return process.communicate(self.content)[0]

    @property
    async def replies(self) -> List[Entry]:
        return []
