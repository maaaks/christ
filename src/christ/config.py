from dataclasses import dataclass, field
from pathlib import Path

import yaml


@dataclass
class _Config_Database:
    hostname: str = None
    port: int = None
    username: str = None
    password: str = None
    database: str = None


@dataclass
class _Config_Paths:
    root: Path = None
    css: Path = None
    data: Path = None
    html: Path = None
    img: Path = None
    sql: Path = None


@dataclass
class _Config:
    database: _Config_Database = field(default_factory=_Config_Database)
    paths: _Config_Paths = field(default_factory=_Config_Paths)

    def __post_init__(self):
        with open('christ.yaml') as file:
            data = yaml.load(file.read(), Loader=yaml.SafeLoader)

            d = data.get('database', {})
            self.database.hostname = d.get('hostname', 'localhost')
            self.database.port = d.get('port', 5432)
            self.database.username = d.get('username')
            self.database.password = d.get('password')
            self.database.database = d.get('database')

            d = data.get('paths', {})
            root = Path(__file__).parent.parent.parent.absolute().resolve()
            self.paths.css = root / d.get('css', 'style.css')
            self.paths.data = root / d.get('data', 'data')
            self.paths.html = root / d.get('html', 'html')
            self.paths.img = root / d.get('img', 'img')
            self.paths.sql = root / d.get('sql', 'sql')


CONFIG = _Config()
