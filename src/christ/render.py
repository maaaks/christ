from datetime import datetime

import jinja2
from aiohttp import web

from christ.config import CONFIG


loader = jinja2.FileSystemLoader(str(CONFIG.paths.html))
env = jinja2.Environment(loader=loader, enable_async=True, undefined=jinja2.StrictUndefined)

async def render(template_name: str, /, **variables) -> web.Response:
    from christ.app import app

    template = env.get_template(template_name)
    html = await template.render_async(**variables, app=app, now=datetime.now)
    return web.Response(content_type='text/html', text=html)
