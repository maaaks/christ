.PHONY: style.css

style.css:
	lessc --rootpath=/ --strict-units=on --clean-css='--s1 --advanced' less/_style.less style.css
