##########################################
# GitHub Flavored Markdown

FROM debian:buster AS cmark-gfm
RUN apt update
RUN apt install -y wget cmake make g++
WORKDIR /src
RUN wget -q -O- https://github.com/github/cmark-gfm/archive/0.29.0.gfm.0.tar.gz | tar -xz --strip-components=1
WORKDIR /build
RUN cmake /src -DCMARK_TESTS=OFF -DCMARK_STATIC=ON -DCMARK_SHARED=OFF
RUN make -j`nproc`
RUN make install DESTDIR=/dist
RUN strip /dist/usr/local/bin/cmark-gfm


##########################################
# LESS

FROM node:14 AS css
RUN npm install -g less less-plugin-clean-css
COPY Makefile .
COPY less less
RUN make style.css


##########################################

FROM python:3.8-buster AS orm
WORKDIR /orm
COPY lib/orm .
RUN python setup.py sdist


##########################################

FROM python:3.8-buster
COPY --from=orm /orm/dist /dependencies

WORKDIR /christ
RUN mkdir src
COPY setup.py .
RUN pip install -e . --find-links=/dependencies
RUN rm -rf /dependencies

COPY --from=css style.css .
COPY img img
COPY html html
COPY src src

COPY --from=cmark-gfm /dist /

CMD src/christ/app.py
