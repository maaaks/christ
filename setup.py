#!/usr/bin/env python

from distutils.core import setup


setup(
    name='christ',
    version='0.0.1',
    author='Max Alibaev',
    python_requires='~=3.8',
    install_requires=[
        'aiohttp~=3.6',
        'asyncpg~=0.20.1',
        'jinja2~=2.11',
        'pyyaml~=5.3.1',
    ],
    package_dir={
        '': 'src',
        'orm': 'lib/orm/src',
    },
    entry_points={
        'console_scripts': ['christ=christ.app:main'],
    },
)
