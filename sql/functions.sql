create or replace function count_comments(given_id bigint) returns bigint as
$$
	with recursive roots(id, root_id) as (
		select id, id from entries
		where id=given_id
	union
		select entries.id, roots.root_id from entries, roots
		where entries.parent_id = roots.id
	)
	select count(id) from roots
	where id != root_id
$$
language sql;
